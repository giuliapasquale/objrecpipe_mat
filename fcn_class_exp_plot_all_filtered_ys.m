function fcn_class_exp_plot_all_filtered_ys(machine, dataset_name, modality, task, classification_kind, feature_names)

close all;

%% MACHINE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%machine = 'server';
%machine = 'laptop_giulia_win';
%machine = 'laptop_giulia_lin';
    
if strcmp(machine, 'server')
    
    FEATURES_DIR = '/home/icub/GiuliaP/objrecpipe_mat';
    root_path = '/DATA/DATASETS';
    
    run('/home/icub/Dev/GURLS/gurls/utils/gurls_install.m');
    
    curr_dir = pwd;
    cd([getenv('VLFEAT_ROOT') '/toolbox']);
    vl_setup;
    cd(curr_dir);
    clear curr_dir;
    
elseif strcmp (machine, 'laptop_giulia_win')
    
    FEATURES_DIR = 'C:\Users\Giulia\REPOS\objrecpipe_mat';
    root_path = 'D:\DATASETS';
    
elseif strcmp (machine, 'laptop_giulia_lin')
    
    FEATURES_DIR = '/home/giulia/REPOS/objrecpipe_mat';
    root_path = '/media/giulia/DATA/DATASETS';
    
    curr_dir = pwd;
    cd([getenv('VLFEAT_ROOT') '/toolbox']);
    vl_setup;
    cd(curr_dir);
    clear curr_dir;
end

addpath(genpath(FEATURES_DIR));

check_input_dir(root_path);

%% DATASET %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%dataset_name = 'Groceries_4Tasks';
%dataset_name = 'Groceries';
%dataset_name = 'Groceries_SingleInstance';
%dataset_name = 'iCubWorld0';
%dataset_name = 'iCubWorld20';
%dataset_name = 'iCubWorld30';
%dataset_name = 'prova';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ICUBWORLDopts = ICUBWORLDinit(dataset_name);

cat_names = keys(ICUBWORLDopts.categories);
obj_names = keys(ICUBWORLDopts.objects)';
tasks = keys(ICUBWORLDopts.tasks);
modalities = keys(ICUBWORLDopts.modalities);

Ncat = ICUBWORLDopts.categories.Count;
Nobj = ICUBWORLDopts.objects.Count;
NobjPerCat = ICUBWORLDopts.objects_per_cat;

%% MODALITY %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%modality = 'carlo_household_right';
%modality = 'human';
%modality = 'robot';
%modality = 'lunedi22';
%modality = 'martedi23';
%modality = 'mercoledi24';
%modality = 'venerdi26';
%modality = '';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~isempty(modality) && sum(strcmp(modality, modalities))==0
    error('Modality does not match any existing modality.');
end

%% TASK %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%task = 'background';
%task = 'categorization';
%task = 'demonstrator';
%task = 'robot';
%task = '';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~isempty(task) && sum(strcmp(task, tasks))==0
    error('Task does not match any existing task.');
end

%% CLASSIFICATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%classification_kind = 'obj_rec_random_nuples';
%classification_kind = 'obj_rec_inter_class';
%classification_kind = 'obj_rec_intra_class';
%classification_kind = 'categorization';

%% FEATURES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%feature_names = {'sc_d1024_iros', 'overfeat_small_default'};
%feature_names = {'sc_d512', 'overfeat_small_default'};
%feature_names = {'sc_d1024_iros'};
%feature_names = {'sc_d512'};
%feature_names = {'overfeat_small_default'};
%feature_names = {'caffe_prova', 'overfeat_small_default', 'sc_d512'};
%feature_names = {'caffe_centralcrop_meanimagenet2012', 'overfeat_small_default', 'sc_d512'};

feature_number = length(feature_names);

%% MAIL SETUP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run('setup_mail_matlab.m');

mail_recipient = {'giu.pasquale@gmail.com'};
mail_object = [mfilename '_' dataset_name];
mail_message = 'Successfully executed.';

%% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

working_folder = fullfile(root_path, [dataset_name '_experiments'], classification_kind);
check_input_dir(working_folder);

if isempty(task) && isempty(modality)
    filtered_ys_filename = 'saved_output_filtered.mat';
elseif isempty(modality)
     filtered_ys_filename = ['saved_output_filtered_' task '.mat'];
elseif isempty(task)
     filtered_ys_filename = ['saved_output_filtered_' modality '.mat'];
else
    filtered_ys_filename = ['saved_output_filtered_' modality '_' task '.mat'];
end

%% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figures_dir = fullfile(working_folder, 'figures');
check_output_dir(figures_dir);

figure_name_prefix = cell2mat(strcat(feature_names', '_')');
if ~isempty(modality) && ~isempty(task)
    figure_name_prefix = [figure_name_prefix modality '_' task '_'];
elseif isempty(modality)
    figure_name_prefix = [figure_name_prefix task '_'];
elseif isempty(task)
    figure_name_prefix = [figure_name_prefix modality '_'];
end

%% FILTERING PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%windows = [1:20 24:4:50];
%fps = 7.5;
windows = [1:20 25:5:55];
fps = 11;

dt_frame = 1/fps; % sec
temporal_windows = windows*dt_frame;
nwindows = length(windows);

%% PLOTTING PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%n1_tobeplotted = [2 7 14 28];
n1_tobeplotted = 7;

n2_tobeplotted = 1;
n3_tobeplotted = 1; % set to 1 if exp_folder = 'obj_rec_random_nuples'
% either n2 or n3 to be plotted must be a scalar

windows_leg = [round(1/33*100)/100; round(temporal_windows(:)*10)/10];

std_factor = 0.25;

%% CODE EXECUTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% compute accuracies

mean_accuracy_over_classes = cell(feature_number, 1);
mean_accuracy_over_nuples = cell(feature_number, 1);
var_accuracy_over_nuples = cell(feature_number, 1);

if strcmp(classification_kind, 'obj_rec_intra_class')
    mean_accuracy_over_nuples_perclass = cell(feature_number, 1);
    var_accuracy_over_nuples_perclass = cell(feature_number, 1);
elseif strcmp(classification_kind, 'categorization')
    mean_accuracy_over_nuples_perclass = cell(feature_number, 1);
    var_accuracy_over_nuples_perclass = cell(feature_number, 1);
end

for feat_idx=1:length(feature_names)

    % compute mean accuracies over nuples

    loaded_ys = load(fullfile(working_folder, feature_names{feat_idx}, filtered_ys_filename));
    cell_output = loaded_ys.cell_output;
    
    N1 = length(cell_output);
    
    mean_accuracy_over_classes{feat_idx} = cell(N1,1);
    mean_accuracy_over_nuples{feat_idx} = zeros(N1, nwindows+1);
    var_accuracy_over_nuples{feat_idx} = zeros(N1, nwindows+1);

    if strcmp(classification_kind, 'obj_rec_intra_class')
        % in this case N3 should be equal to Ncat for each n1
        mean_accuracy_over_nuples_perclass{feat_idx} = zeros(N1, Ncat, nwindows+1); 
        var_accuracy_over_nuples_perclass{feat_idx} = zeros(N1, Ncat, nwindows+1);
    end
    
    % extract accuracy information from cell array of results
    for n1=1:N1
        
        [N2, N3] = size(cell_output{n1});   
        
        mean_accuracy_over_classes{feat_idx}{n1} = zeros(N2, N3, nwindows+1);
        
        for n2=1:N2
            for n3=1:N3 
                if isfield(cell_output{n1}{n2, n3}, 'accuracy_mode_perclass')
                    mean_accuracy_over_classes{feat_idx}{n1}(n2, n3, :) = [mean(cell_output{n1}{n2, n3}.accuracy); mean(cell_output{n1}{n2, n3}.accuracy_mode_perclass,1)'];
                else
                    mean_accuracy_over_classes{feat_idx}{n1}(n2, n3, :) = [mean(cell_output{n1}{n2, n3}.accuracy); cell_output{n1}{n2, n3}.accuracy_mode'];
                end
            end
        end
        
        if strcmp(classification_kind, 'categorization') && n1==N1
            mean_accuracy_over_nuples_perclass{feat_idx} = zeros(Ncat, nwindows);
            var_accuracy_over_nuples_perclass{feat_idx} = zeros(Ncat, nwindows);
        end
        
        for w=1:nwindows+1
            
            mean_accuracy_over_nuples{feat_idx}(n1, w) = mean2(squeeze(mean_accuracy_over_classes{feat_idx}{n1}(:,:,w)));
            var_accuracy_over_nuples{feat_idx}(n1, w) = std2(squeeze(mean_accuracy_over_classes{feat_idx}{n1}(:,:,w)));
           
            if strcmp(classification_kind, 'obj_rec_intra_class')
                for n3=1:N3
                    mean_accuracy_over_nuples_perclass{feat_idx}(n1, n3, w) = mean(squeeze(mean_accuracy_over_classes{feat_idx}{n1}(:,n3,w)));
                    var_accuracy_over_nuples_perclass{feat_idx}(n1, n3, w) = std(squeeze(mean_accuracy_over_classes{feat_idx}{n1}(:,n3,w)));
                end
            elseif strcmp(classification_kind, 'categorization') && n1==N1 && w<nwindows+1
                for idx_cat=1:Ncat
                    tmp = cell2mat(cell_output{N1});
                    tmp = cat(3, tmp.accuracy_mode_perclass);
                    mean_accuracy_over_nuples_perclass{feat_idx}(idx_cat, w) = mean( tmp(idx_cat, w, :) );
                    var_accuracy_over_nuples_perclass{feat_idx}(idx_cat, w) = mean( tmp(idx_cat, w, :) );
                end
            end
            
        end
        
    end
    
end

% plot

for feat_idx=1:length(feature_names)
   
    % ys
    
    loaded_ys = load(fullfile(working_folder, feature_names{feat_idx}, filtered_ys_filename));
    cell_output = loaded_ys.cell_output;
    
    for n1=n1_tobeplotted
        
        figure(n1)
        set(gcf,'units','normalized','outerposition',[0 0 1 1])
        
        subplot(1,length(feature_names),feat_idx)
        nsamples = size(cell_output{n1-1}{n2_tobeplotted, n3_tobeplotted}.y, 1);
        
        plotted_mat = zeros(nsamples, nwindows+2);
        plotted_mat(:,1) = cell_output{n1-1}{n2_tobeplotted, n3_tobeplotted}.ypred;
        plotted_mat(:,end) = cell_output{n1-1}{n2_tobeplotted, n3_tobeplotted}.y;
        
        for idx_wndw=1:nwindows
            ypred_mode = cell_output{n1-1}{n2_tobeplotted, n3_tobeplotted}.ypred_mode{idx_wndw};
            plotted_mat(:,1+idx_wndw) = ypred_mode(:);
            
        end
        
        imagesc(plotted_mat);
        colormap(jet(n1+1));
        colorbar_labels = '0 pad'; 
        colorbar_labels = [colorbar_labels; cell_output{n1-1}{n2_tobeplotted, n3_tobeplotted}.experiment];
        if (feat_idx==length(feature_names))
            h = colorbar('YLim', [0 n1], 'YTick', (0.5+[0; unique(plotted_mat(:,end))])*n1/(n1+1), 'YTickLabel', colorbar_labels);
            set(h, 'FontSize', 13.5);
            set( h, 'YDir', 'reverse' );
        end
        set(gca, 'XTick', 1:(nwindows+1));
        xticklabels = cellstr(num2str(windows_leg));
        for id=2:4:numel(xticklabels)
            xticklabels{id} = [];
            xticklabels{id+1} = [];
            xticklabels{id+2} = [];
        end
        xticklabels{end} = num2str(windows_leg(end));
        set(gca, 'XTickLabel', xticklabels, 'FontSize', 15);
        ylabel('samples', 'FontSize', 20);
        xlabel('time window [s]', 'FontSize', 20);
        title(strrep(feature_names{feat_idx}, '_', ' ' ), 'FontSize', 20);
    end
    
    % accuracies 
    
    figure(1)
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    subplot(1,length(feature_names),feat_idx)
    
    set(0,'DefaultAxesColorOrder',jet(nwindows+1))
    plot(2:N1+1, squeeze(mean_accuracy_over_nuples{feat_idx}));
    if (feat_idx==1)
        legend(strcat(cellstr(num2str(windows_leg)), 's'), 'Location', 'Best', 'FontSize', 13.5);
    end
    
    wndw_idx = 1;
%     for wndw_idx=1:nwindows+1
         errorfill(2:N1+1, mean_accuracy_over_nuples{feat_idx}(:, wndw_idx)', var_accuracy_over_nuples{feat_idx}(:, wndw_idx)'*std_factor);
%     end

    hold on
    plot(2:N1+1, squeeze(mean_accuracy_over_nuples{feat_idx}));
    grid on, box on
    xlabel('classes', 'FontSize', 20);
    set(gca, 'XTick', 2:N1+1);
    xticklabels = (2:N1+1)';
    xticklabels = cellstr(num2str(xticklabels));
    for id=2:2:numel(xticklabels)
        xticklabels{id} = [];
    end
    set(gca, 'XTickLabel', xticklabels);
    set(gca, 'FontSize', 15);
    if (feat_idx==1)
        ylabel('accuracy (mean over classes)', 'FontSize', 20);
    end
    if (feat_idx==2)
        yticklabels = cellstr(get(gca, 'YTickLabel'));
        for id=1:numel(yticklabels)
            yticklabels{id} = [];
        end
        set(gca, 'YTickLabel', yticklabels);
    end
    
    xlim([2 N1+1]);
    ylim([0 1]);
    title(strrep(feature_names{feat_idx}, '_', ' ' ), 'FontSize', 20);
    
    if strcmp(classification_kind, 'obj_rec_intra_class')

        for n3=1:N3
            
            figure(max(n1_tobeplotted) + n3)
            set(gcf,'units','normalized','outerposition',[0 0 1 1])
        
            subplot(1,length(feature_names),feat_idx)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            set(0,'DefaultAxesColorOrder',jet(nwindows+1))
            plot(2:N1+1, squeeze(mean_accuracy_over_nuples_perclass{feat_idx}(:,n3,:)));
            if (feat_idx==1)
                legend(strcat(cellstr(num2str(windows_leg)), 's'), 'Location', 'Best', 'FontSize', 13.5);
            end
            
            wndw_idx = 1;
            % for wndw_idx=1:nwindows+1
            errorfill(2:N1+1, mean_accuracy_over_nuples_perclass{feat_idx}(:, n3, wndw_idx)', var_accuracy_over_nuples_perclass{feat_idx}(:, n3, wndw_idx)'*std_factor);
            % end
            
            hold on
            plot(2:N1+1, squeeze(mean_accuracy_over_nuples_perclass{feat_idx}(:,n3,:)));
            grid on, box on
            xlabel('classes', 'FontSize', 20);
            set(gca, 'XTick', 2:N1+1);
            xticklabels = (2:N1+1)';
            xticklabels = cellstr(num2str(xticklabels));
            for id=2:2:numel(xticklabels)
                xticklabels{id} = [];
            end
            set(gca, 'XTickLabel', xticklabels);
            set(gca, 'FontSize', 15);
            if (feat_idx==1)
                ylabel('accuracy (mean over classes)', 'FontSize', 20);
            end
            if (feat_idx==2)
                yticklabels = cellstr(get(gca, 'YTickLabel'));
                for id=1:numel(yticklabels)
                    yticklabels{id} = [];
                end
                set(gca, 'YTickLabel', yticklabels);
            end
            
            xlim([2 N1+1]);
            ylim([0 1]);
            title({strrep(feature_names{feat_idx}, '_', ' ' ); cat_names{n3}}, 'FontSize', 20);

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
        end
        
    elseif strcmp(classification_kind, 'categorization')
        
            figure(max(n1_tobeplotted) + 1)
            set(gcf,'units','normalized','outerposition',[0 0 1 1])
            
            subplot(1,length(feature_names),feat_idx)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            set(0,'DefaultAxesColorOrder',jet(nwindows+1))
            plot(1:Ncat, mean_accuracy_over_nuples_perclass{feat_idx});
            if (feat_idx==1)
                legend(strcat(cellstr(num2str(windows_leg)), 's'), 'Location', 'Best', 'FontSize', 13.5);
            end
            
            wndw_idx = 1;
            % for wndw_idx=1:nwindows+1
            errorfill(1:Ncat, mean_accuracy_over_nuples_perclass{feat_idx}(:, wndw_idx)', var_accuracy_over_nuples_perclass{feat_idx}(:, wndw_idx)'*std_factor);
            % end
            
            hold on
            plot(1:Ncat, mean_accuracy_over_nuples_perclass{feat_idx});
            grid on, box on
            xlabel('classes', 'FontSize', 20);
            set(gca, 'XTick', 1:Ncat);
            %xticklabels = (1:Ncat)';
            %xticklabels = cellstr(num2str(xticklabels));
            %for id=2:2:numel(xticklabels)
            %    xticklabels{id} = [];
            %end
            xticklabels = cat_names;
            set(gca, 'XTickLabel', xticklabels);
            set(gca, 'XTickLabelRotation', 45);
            set(gca, 'FontSize', 15);
            if (feat_idx==1)
                ylabel('accuracy (mean over classes)', 'FontSize', 20);
            end
            if (feat_idx==2)
                yticklabels = cellstr(get(gca, 'YTickLabel'));
                for id=1:numel(yticklabels)
                    yticklabels{id} = [];
                end
                set(gca, 'YTickLabel', yticklabels);
            end
            
            xlim([1 Ncat]);
            ylim([0 1]);
            title(strrep(feature_names{feat_idx}, '_', ' ' ), 'FontSize', 20);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%

    end
    
end

% save figures

h = figure(1);
saveas(h, fullfile(figures_dir, [figure_name_prefix 'filtered_acc.fig']));
set(h,'PaperOrientation','landscape');
set(h,'PaperUnits','normalized');
set(h,'PaperPosition', [0 0 1 1]);
% set(gca,'InvertHardcopy','off')
print(h, '-dpdf', fullfile(figures_dir, [figure_name_prefix 'filtered_acc.pdf']));
print(h, '-dpng', fullfile(figures_dir, [figure_name_prefix 'filtered_acc.png']));

for n1=n1_tobeplotted
    
    saveas(figure(n1), fullfile(figures_dir, [figure_name_prefix 'filtered_ypred_nc_' num2str(n1) '_nu_' num2str(n2_tobeplotted) '.fig']));
    h = figure(n1);
    set(h,'PaperOrientation','landscape');
    set(h,'PaperUnits','normalized');
    set(h,'PaperPosition', [0 0 1 1]);
    % set(gca,'InvertHardcopy','off')
    print(h, '-dpdf', fullfile(figures_dir, [figure_name_prefix 'filtered_ypred_nc_' num2str(n1) '_nu_' num2str(n2_tobeplotted) '.pdf']));
    print(h, '-dpng', fullfile(figures_dir, [figure_name_prefix 'filtered_ypred_nc_' num2str(n1) '_nu_' num2str(n2_tobeplotted) '.png']));
end

if strcmp(classification_kind, 'obj_rec_intra_class')
    for n3=1:N3
        
        saveas(figure(max(n1_tobeplotted) + n3), fullfile(figures_dir, [figure_name_prefix 'filtered_acc_' cat_names{n3} '.fig']));
        h = figure(max(n1_tobeplotted) + n3);
        set(h,'PaperOrientation','landscape');
        set(h,'PaperUnits','normalized');
        set(h,'PaperPosition', [0 0 1 1]);
        % set(gca,'InvertHardcopy','off')
        print(h, '-dpdf', fullfile(figures_dir, [figure_name_prefix 'filtered_acc_' cat_names{n3} '.pdf']));
        print(h, '-dpng', fullfile(figures_dir, [figure_name_prefix 'filtered_acc_' cat_names{n3}  '.png']));
    end
    
elseif strcmp(classification_kind, 'categorization')
    h = figure(max(n1_tobeplotted) + 1);
    saveas(h, fullfile(figures_dir, [figure_name_prefix 'filtered_acc_perclass.fig']));
    set(h,'PaperOrientation','landscape');
    set(h,'PaperUnits','normalized');
    set(h,'PaperPosition', [0 0 1 1]);
    % set(gca,'InvertHardcopy','off')
    print(h, '-dpdf', fullfile(figures_dir, [figure_name_prefix 'filtered_acc_perclass.pdf']));
    print(h, '-dpng', fullfile(figures_dir, [figure_name_prefix 'filtered_acc_perclass.png']));
end