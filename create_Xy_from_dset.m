loader = Features.GenericFeature();

ICUBWORLDopts = ICUBWORLDinit('iCubWorld30');
obj_names = keys(ICUBWORLDopts.objects)';


loader.load_feat('/media/giulia/DATA/DATASETS/iCubWorld30_experiments/caffe_centralcrop_meanimagenet2012/test/lunedi22', [], '.mat', [], '/media/giulia/DATA/test_lun.txt');
y_1 = create_y(loader.Registry, obj_names, []);
[~, y] = max(y_1, [], 2);
save('/media/giulia/DATA/Yte_lun.mat', 'y');

%loader.save_feat_matrix('/media/giulia/DATA/Xte_lun.mat');


loader.load_feat('/media/giulia/DATA/DATASETS/iCubWorld30_experiments/caffe_centralcrop_meanimagenet2012/test/martedi23', [], '.mat', [], '/media/giulia/DATA/test_mar.txt');
y_1 = create_y(loader.Registry, obj_names, []);
[~, y] = max(y_1, [], 2);
save('/media/giulia/DATA/Yte_mar.mat', 'y');

%loader.save_feat_matrix('/media/giulia/DATA/Xte_mar.mat');


loader.load_feat('/media/giulia/DATA/DATASETS/iCubWorld30_experiments/caffe_centralcrop_meanimagenet2012/test/mercoledi24', [], '.mat', [], '/media/giulia/DATA/test_mer.txt');
y_1 = create_y(loader.Registry, obj_names, []);
[~, y] = max(y_1, [], 2);
save('/media/giulia/DATA/Yte_mer.mat', 'y');

%loader.save_feat_matrix('/media/giulia/DATA/Xte_mer.mat');


loader.load_feat('/media/giulia/DATA/DATASETS/iCubWorld30_experiments/caffe_centralcrop_meanimagenet2012/test/venerdi26', [], '.mat', [], '/media/giulia/DATA/test_ven.txt');
y_1 = create_y(loader.Registry, obj_names, []);
[~, y] = max(y_1, [], 2);
save('/media/giulia/DATA/Yte_ven.mat', 'y');

%loader.save_feat_matrix('/media/giulia/DATA/Xte_ven.mat');



loader.load_feat('/media/giulia/DATA/DATASETS/iCubWorld30_experiments/caffe_centralcrop_meanimagenet2012/train/lunedi22', [], '.mat', [], '/media/giulia/DATA/train_lun.txt');
y_1 = create_y(loader.Registry, obj_names, []);
[~, y] = max(y_1, [], 2);
save('/media/giulia/DATA/Ytr_lun.mat', 'y');

%loader.save_feat_matrix('/media/giulia/DATA/Xtr_lun.mat');


loader.load_feat('/media/giulia/DATA/DATASETS/iCubWorld30_experiments/caffe_centralcrop_meanimagenet2012/train/martedi23', [], '.mat', [], '/media/giulia/DATA/train_mar.txt');
y_1 = create_y(loader.Registry, obj_names, []);
[~, y] = max(y_1, [], 2);
save('/media/giulia/DATA/Ytr_mar.mat', 'y');

%loader.save_feat_matrix('/media/giulia/DATA/Xtr_mar.mat');


loader.load_feat('/media/giulia/DATA/DATASETS/iCubWorld30_experiments/caffe_centralcrop_meanimagenet2012/train/mercoledi24', [], '.mat', [], '/media/giulia/DATA/train_mer.txt');
y_1 = create_y(loader.Registry, obj_names, []);
[~, y] = max(y_1, [], 2);
save('/media/giulia/DATA/Ytr_mer.mat', 'y');

%loader.save_feat_matrix('/media/giulia/DATA/Xtr_mer.mat');


loader.load_feat('/media/giulia/DATA/DATASETS/iCubWorld30_experiments/caffe_centralcrop_meanimagenet2012/train/venerdi26', [], '.mat', [], '/media/giulia/DATA/train_ven.txt');
y_1 = create_y(loader.Registry, obj_names, []);
[~, y] = max(y_1, [], 2);
save('/media/giulia/DATA/Ytr_ven.mat', 'y');

%loader.save_feat_matrix('/media/giulia/DATA/Xtr_ven.mat');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            opt = defopt('optprova');
            
            opt.hoproportion = 0.5;
            opt.nlambda = 20;
            opt.kernel.type = 'linear';
            opt.paramsel.hoperf = @perf_macroavg;
            opt.seq = {'kernel:linear','split:ho', 'paramsel:hodual', 'rls:dual', 'pred:dual', 'perf:macroavg'};
            
            opt.process{1} = [2,2,2,2,0,0];
            opt.process{2} = [3,3,3,3,2,2];
            
            % train
            
            Xtr = load('Xtr_lun.mat');
            Xtr = Xtr.feat';
            ytrclass = load('Ytr_lun.mat');
            ytr = -ones(size(Xtr,1),max(ytrclass.y));
            ytr( sub2ind(size(ytr),1:size(Xtr,1),ytrclass.y') ) = 1;
            Xm = mean(Xtr,1);
            Xtr = Xtr - ones(size(Xtr,1),1)*Xm;
            
            gurls (Xtr, ytr, opt, 1);
            
            % test
            
            Xte = load('Xte_lun.mat');
            Xte = Xte.feat';
            yteclass = load('Yte_lun.mat');
            yte = -ones(size(Xte,1),max(yteclass.y));
            yte( sub2ind(size(yte),1:size(Xte,1),yteclass.y') ) = 1;
            Xte = Xte - ones(size(Xte,1),1)*Xm;
            
            gurls (Xte, yte, opt, 2);
            clear Xte sc_test.Feat;