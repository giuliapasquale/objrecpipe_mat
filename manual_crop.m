
FEATURES_DIR = 'D:/objrecpipe_mat';
addpath(genpath(FEATURES_DIR));
    
in_root_dir = 'D:/DATASETS/iCubWorld30/train/venerdi26';
check_input_dir(in_root_dir);

out_root_dir = 'D:/DATASETS/iCubWorld30_manually_cropped/train/venerdi26';
check_output_dir(out_root_dir);

reg_file = 'D:/DATASETS/iCubWorld30_manually_cropped/registries/registry_train_ven.txt';

dset = Features.GenericFeature();

dset.assign_registry_and_tree_from_folder(in_root_dir, [], reg_file);

dset.reproduce_tree(out_root_dir);

h = figure(1)

%set(h,'KeyPressFcn',@object.getKeyPressOnFigure);
 
for im=2364:dset.ExampleCount
    
    I = imread(fullfile(in_root_dir, [dset.Registry{im} '.ppm']));
   
    h = figure(1)
    set(h,'units','normalized','outerposition',[0 0 3/4 1])

    Icrop = imcrop(I);

    imwrite(Icrop, fullfile(out_root_dir, [dset.Registry{im} '.ppm']));
    im
    close(h)
    
end