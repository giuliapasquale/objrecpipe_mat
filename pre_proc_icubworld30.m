%% MATLAB path update

% add the folder containing the '+Features' package to the path
FEATURES_DIR = 'C:\Users\Giulia\REPOS\objrecpipe_mat';
addpath(FEATURES_DIR);

% add the current working directory to the path
addpath(genpath('.'));

% common root path
root_path = 'D:\IIT';

datasets_folder = fullfile(root_path, 'DATASETS');

%% iCubWorld3.0 source folder

version = 3;
%modality = 'lunedi22';
%modality = 'martedi23';
%modality = 'mercoledi24';
modality = 'venerdi26';

% set dataset directory
dataset_path = fullfile(datasets_folder, 'iCubWorld30_preprocessing', modality);

% name of input files from dumper
time_img = 'imgs.log'; % contains timestamp + img name
time_info = 'imginfos.log'; % contains timestamp + blob + class

blob_img = 'blob_img.txt';

% assign image format
dataset_extension = [];

%% Dataset initialization

%imgset = 'test';
imgset = 'train';

% input 
dataset_raw_images_path = fullfile(dataset_path, imgset);
time_img_path = fullfile(dataset_raw_images_path, time_img);
time_info_path = fullfile(dataset_raw_images_path, time_info);

% output
dataset_images_path = fullfile(dataset_path, 'PPMImages', imgset);
blob_img_path = fullfile(dataset_raw_images_path, blob_img);

dataset_raw = Features.MyDataset(dataset_extension);

box_size = 63; % half side of the bounding box
% e.g. 127 to crop 2x127+1+1 = 256 squared images

dataset_raw.segment_30(dataset_raw_images_path, dataset_images_path, time_img_path, time_info_path, blob_img_path, box_size);
